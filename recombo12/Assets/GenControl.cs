﻿using UnityEngine;
using System.Collections;

public class GenControl : MonoBehaviour {
	bool hasBeen=false;
	[SerializeField] Transform genPlace;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate (-0.2f, 0, 0);
		if (transform.position.x <= GameObject.Find ("robo1").GetComponent<playermove> ().stageDimensions.x && !hasBeen) {
			Instantiate (gameObject, (Vector2)genPlace.position + Vector2.right /** Random.Range (0, 3)*/, Quaternion.identity);
			hasBeen = true;
		}
	}
}
