﻿using UnityEngine;
using System.Collections;

public class playermove : MonoBehaviour
{
    public float speed;
	float startSpeed;
    public float jumppower;
    private Rigidbody2D myrigidbody;
	public bool collidertouch;
    public bool grounded;
    public LayerMask whatIsGraund;
	public LayerMask whatIsBound;
    private Collider2D myCollider;
	public Vector3 stageDimensions;
    // Use this for initialization
    void Start()
    {
        myrigidbody = GetComponent<Rigidbody2D>();
        myCollider = GetComponent<Collider2D>();
		startSpeed = speed;

    }

    // Update is called once per frame
    void Update()
    {
		stageDimensions = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height,0));
        grounded = Physics2D.IsTouchingLayers(myCollider, whatIsGraund);

		collidertouch = Physics2D.IsTouchingLayers (myCollider, whatIsBound);
		if (Input.GetKey(KeyCode.RightArrow))

      /*  if (Input.GetKey(KeyCode.RightArrow))
>>>>>>> a7513f7f9f5d1bc5e7c0837d418c06d4c7a0d555
            myrigidbody.velocity = new Vector2(speed, myrigidbody.velocity.y);
        if (Input.GetKeyUp(KeyCode.RightArrow))
            myrigidbody.velocity = new Vector2(0, myrigidbody.velocity.y);
		if (Input.GetKey(KeyCode.LeftArrow))
            myrigidbody.velocity = new Vector2(speed * (-1), myrigidbody.velocity.y);
        if (Input.GetKeyUp(KeyCode.LeftArrow))
            myrigidbody.velocity = new Vector2(0, myrigidbody.velocity.y);*/
	
		if (transform.position.x >= stageDimensions.x)
		{
			if (Input.GetAxis ("Horizontal") > 0)
				speed = 0;
			if (speed==0 && Input.GetAxis ("Horizontal") <= 0)
				speed = startSpeed;
		}
		
		if (transform.position.x <= -stageDimensions.x) {
			if (Input.GetAxis ("Horizontal") < 0)
				speed = 0;
			if (speed == 0 && Input.GetAxis ("Horizontal") >= 0)
				speed = startSpeed;
		}
		
			

		myrigidbody.velocity = new Vector2 (Input.GetAxis ("Horizontal")*speed, myrigidbody.velocity.y);

        if (Input.GetKeyDown(KeyCode.Space) && grounded == true)
        {
                myrigidbody.velocity = new Vector2(myrigidbody.velocity.x, jumppower);
        }
    }
}
