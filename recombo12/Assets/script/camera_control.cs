﻿using UnityEngine;
using System.Collections;

public class camera_control : MonoBehaviour {
	public playermove thePlayer;
    public Camera myCamera;

	private Vector3 lastplayerposition;
	private float distancetoMove;
	// Use this for initialization
	void Start () {
		thePlayer = FindObjectOfType<playermove>();
        myCamera = FindObjectOfType<Camera>();
		lastplayerposition = thePlayer.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		distancetoMove = thePlayer.transform.position.x - lastplayerposition.x;
		if (distancetoMove>0)
		transform.position = new Vector3 (transform.position.x + distancetoMove, transform.position.y, transform.position.z);
		lastplayerposition = thePlayer.transform.position;

	}
}
